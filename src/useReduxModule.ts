import { Reducer } from "redux";
import { Saga } from "redux-saga";
import { fork, take, cancel, ForkEffect, TakeEffect, CancelEffect } from "redux-saga/effects";
import { useEffect } from "react";
import { store } from './index';
import { sagaMiddleware } from './store';
import { useDispatch } from "react-redux";

export const STOP = "STOP_SAGA"

const startSaga = (key: string, saga: Saga) => {
    const runnableSaga: any = function* main() {
        const task = yield fork(saga); // Forks the saga
        const { payload } = yield take(STOP); // Listen for cancel action

        if (payload === key) { yield cancel(task); }
    };

    sagaMiddleware.run(runnableSaga);
};

const stopSaga = (key: string) => {
    store.dispatch({ type: STOP, payload: key});
};

const useReduxModule = (
    key: string,
    reducer: Reducer, 
    saga?: Saga, 
    initialAction?: any
) => {
    useEffect(() => {
        store.injectReducer(key, reducer);
        if (saga) { startSaga(key, saga); }
        if (initialAction) { store.dispatch(initialAction) }
        return () => {
            stopSaga(key);
            store.removeReducer(key);
        };
    });
};

export default useReduxModule;