import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import HomePage from './components/HomePage';
import NavBar from './components/NavBar';

interface Props {}

class App extends React.Component {
    constructor(props: Props) {
        super(props);
        this.state = {
            uid : null,
        }
    }

    componentWillMount() {
        // TODO CONNECT REDUX
    }

    render () {
        return (
            <div className="App">
                <Router>
                    <NavBar/>
                    <Route path="/" exact component={HomePage} />
                </Router>
            </div>
        );
    }
}

export default App; 