import { RootAction, ActionKeys } from "./types";

const initialState: RootState = {
    sessionToken: null,
    uid: null,
}

export interface RootState {
    sessionToken: string | null;
    uid: string | null;
}

export function RootReducer(
    state = initialState,
    action: RootAction
): RootState {
    switch(action.type) {
        case ActionKeys.updateUser:
            return { ...state, uid: action.uid };
        case ActionKeys.updateSession:
            return { ...state, sessionToken: action.sessionToken };
    }
}