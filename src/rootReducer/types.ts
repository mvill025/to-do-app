export type RootAction =
    | UpdateUser
    | UpdateSession
;
export enum ActionKeys {
    updateUser = "UPDATE_USER", 
    updateSession = "UPDATE_SESSION"
};

export interface UpdateUser {
    type: ActionKeys.updateUser,
    uid: string | null,
};
export interface UpdateSession {
    type: ActionKeys.updateSession,
    sessionToken: string | null,
};

