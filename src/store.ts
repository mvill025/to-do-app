import { createStore, Store, Reducer, combineReducers, applyMiddleware } from "redux";
import createSagaMiddleware from 'redux-saga';
import { RootReducer } from "./rootReducer";

const staticReducers = combineReducers({
    ...RootReducer
})

interface ModularStore extends Store {
    injectReducer: (key: string, reducer: Reducer) => void;
    removeReducer: (key: string) => void;
    asyncReducers: { [key:string]: Reducer }
}

function updateReducers(asyncReducers: Reducer) {
    return combineReducers({
        ...staticReducers,
        ...asyncReducers
    })
}

export const sagaMiddleware = createSagaMiddleware();

export default function configureStore() {
    const store: ModularStore = {
        ...createStore(staticReducers, applyMiddleware(sagaMiddleware)),
        asyncReducers: {},
        injectReducer: (key: string, reducer: Reducer) => {
            store.asyncReducers[key] = reducer;
            const asyncReducers = combineReducers({ ...store.asyncReducers })
            store.replaceReducer(updateReducers(asyncReducers));
        },
        removeReducer: (key: string) => {
            delete store.asyncReducers[key];
            delete store.getState()[key];
        }
    };
    return store;
}