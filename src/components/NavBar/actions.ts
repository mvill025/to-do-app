export type NavBarAction =
    | FetchTabs
;

export enum ActionKeys {
    fetchTabs = "FETCH_TABS"
};

export interface FetchTabs {
    type: ActionKeys.fetchTabs
};
