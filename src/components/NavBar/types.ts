export enum NavTab {
    faqList = "FAQ List",
    createFaq = "Create FAQ"
}

export interface NavBarState {
    tabs: NavTab[]
    selectedTab?: NavTab
}