import { NavBarAction, ActionKeys } from "./actions"
import { NavBarState, NavTab } from './types'

const INITIAL_STATE: NavBarState = {
    tabs: [
        NavTab.faqList,
        NavTab.createFaq,
    ],
    selectedTab: NavTab.faqList
}

export default function NavBarReducer(state = INITIAL_STATE, action: NavBarAction) {
    switch (action.type) {
        case ActionKeys.fetchTabs: return { ...state };
    }
}