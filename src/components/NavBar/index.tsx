import React from 'react'
import { connect } from 'react-redux'
import { Tabs, Tab } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import { NavBarAction, ActionKeys } from './actions'
import useReduxModule from '../../useReduxModule'
import NavBarReducer from './reducer'
import saga from './sagas'
import './index.css';
import { NavTab } from './types';

interface Props {
    tabs?: NavTab[];
    selectedTab?: string;
    onTabSelected?: (event: React.ChangeEvent<{}>, value: any) => void;
    getTabs?: NavBarAction
}

const StyledTabs = withStyles({
    indicator: {
      display: 'flex',
      justifyContent: 'center',
      backgroundColor: 'transparent',
      '& > div': {
        width: '100%',
        backgroundColor: '#ffffff',
      },
    },
})(Tabs);

const NavBar = (props: Props) => {
    const { getTabs, selectedTab, tabs } = props;
    useReduxModule('Nav Bar', NavBarReducer, saga, getTabs);
    return (
        <React.Fragment>
            <div className="root">
                <StyledTabs 
                    className="tabsContainer"
                    value={selectedTab} 
                    indicatorColor="primary"
                    textColor="primary"
                    >
                { tabs?.map(tab =>(<Tab label={tab} value={tab} />))}
                </StyledTabs>
            </div>
        </React.Fragment>
    );
}

export default connect(null, { fetchTabs: {type: ActionKeys.fetchTabs} })(NavBar);